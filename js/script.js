// 1. Створіть об'єкт person з властивостями name, age та gender.
// Далі створіть новий об'єкт newPerson та скопіюйте в нього властивості
// з об'єкту person. Потім змініть властивість age в об'єкті newPerson на нове
// значення та виведіть обидва об'єкти у консоль.
// Об'єкти мають бути різні.


let person = {
    name: 'Kris',
    age: 31,
    gender: 'female',
};


let newPerson = {
    ...person
}

newPerson.age = 35

console.log(person)
console.log(newPerson)

// 2. Створіть об'єкт car з властивостями make, model та year.
// Далі створіть функцію printCarInfo, яка приймає об'єкт car як аргумент
// та виводить у консоль інформацію про машину в форматі
// "Make: {make}, Model: {model}, Year: {year}".
// Якщо рік випуску менше чим 2001, вивести повідомлення "Машина занадто стара.".

let car = {
    make: 'Chevrolet',
    model: 'Orlando',
    year: 2000,
}

function printCarInfo(car) {
    console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}`);
    if (car.year < 2001) {
        console.log("Машина занадто стара.");
    }
}

printCarInfo(car);

// 3. Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію countWords,
// яка приймає рядок тексту як аргумент та повертає кількість слів у рядку.

let str = "Тут може бути ваша реклама !";

function countWords(str) {
    let words = str.split(" ");
    return words.length;
}

console.log(countWords(str));

// Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію reverseString,
// яка приймає рядок тексту як аргумент та повертає його в оберненому порядку.

const str1 = 'JavaScript is awesome!';
    function reverseString(str1) {
    return str1.split('').reverse().join('');

}


console.log(reverseString(str1));



// Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію
// reverseWordsInString, яка приймає рядок тексту як аргумент та повертає
// його з розвернутими словами.

function reverseWordsInString(str2) {
    return str2.split(' ').map(word => word.split('').reverse().join('')).join(' ');
}

let str2 = "Це рядок тексту. Його потрібно перевернути.";
console.log(reverseWordsInString(str2));